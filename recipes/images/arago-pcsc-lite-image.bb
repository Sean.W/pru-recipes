# Arago TI SDK base image
# gives you an image with Qt/E and other common packages shared by all TI SDKs.

require arago-image.inc

COMPATIBLE_MACHINE = "omapl138"

IMAGE_INSTALL += "\
    task-arago-base \
    task-arago-console \
    task-arago-base-tisdk \
    pcsc-lite \
    handlertest \
    pru-smartcard-ifdhandler \
    pru-smartcard-firmware \
    pru-smartcard-examples \
    pru-suart-firmware \
    pru-suart-sc-firmware \
    devmem2 \
    picocom \
    minicom \
    "

export IMAGE_BASENAME = "arago-pcsc-lite-image"

# A couple of special cleanups for the image
# 1.  Remove the /etc/dbus-1/event.d/20hal file because it slows boot for
#     no good reason and is fixed in later toolchains.
# 2.  Create the directory /usr/lib/pcsc/drivers which is required to be
#     present for the pcscd daemon to run.
ROOTFS_POSTPROCESS_COMMAND += "rm ${IMAGE_ROOTFS}/etc/dbus-1/event.d/20hal;"
ROOTFS_POSTPROCESS_COMMAND += "mkdir -p ${IMAGE_ROOTFS}/usr/lib/pcsc/drivers;"
