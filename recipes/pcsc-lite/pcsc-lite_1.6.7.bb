DESCRIPTION = "PC/SC Lite smart card framework and applications"
HOMEPAGE = "http://pcsclite.alioth.debian.org/"
LICENSE = "BSD"

DEPENDS = "hal"
RDEPENDS_${PN} = "hal"

PR = "r2"

SRC_URI = "https://alioth.debian.org/frs/download.php/3516/pcsc-lite-${PV}.tar.bz2 \
           file://pcscd.init "

inherit autotools update-rc.d

INITSCRIPT_NAME = "pcscd"
INITSCRIPT_PARAMS = "defaults"

EXTRA_OECONF = " \
	--enable-libhal \
	--disable-libusb \
	"

do_install() {
	oe_runmake DESTDIR="${D}" install
	install -d "${D}/etc/init.d"
	install -m 755 "${WORKDIR}/pcscd.init" "${D}/etc/init.d/pcscd"
}

PACKAGES =+ "libpcsclite"
PACKAGES =+ "${PN}-collateral"

FILES_libpcsclite = "${libdir}/libpcsclite.so.*"
FILES_${PN}-collateral = "${sysconfdir}/reader.conf.d/*"
CONFFILES_${PN}-collateral = "${sysconfdir}/reader.conf.d/*"

SRC_URI[md5sum] = "62115ae6dc6fc23da7ffe50986e84eb7"
SRC_URI[sha256sum] = "e1797726d1fa89beeba0f91dd727e8817ee85f6027e7e562eca1bacece3b8f62"
