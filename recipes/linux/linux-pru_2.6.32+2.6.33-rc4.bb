SECTION = "kernel"
DESCRIPTION = "Linux kernel for am180x that supports SmartCard and SUART"
LICENSE = "GPLv2"
KERNEL_IMAGETYPE = "uImage"

inherit kernel

MACHINE_KERNEL_PR_append = "o"

BRANCH ?= "master"

SRC_URI = "git://gitorious.org/pru/pru-linux-drivers.git;protocol=git;branch=${BRANCH} \
    file://defconfig"

# Requires support for SOC_FAMILY matching in COMPATIBLE_MACHINE
COMPATIBLE_MACHINE = "am180x-evm"

S = "${WORKDIR}/git"

# Do not include generic linux.inc, but copy defconfig in place
addtask setup_defconfig before do_configure after do_patch
do_setup_defconfig() {
    cp ${WORKDIR}/defconfig ${S}/.config
}
KVER = "2.6.33-rc4"
PSPREL = "03.20.00.12.pru"

SRCREV = "57c7ce647775850c5d008ece2f12e0e0384cac99"
