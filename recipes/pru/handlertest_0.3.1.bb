DESCRIPTION = "IFDHandler test code"
HOMEPAGE = "https://alioth.debian.org/projects/pcsclite/"
LICENSE = "GPLv2+"
PRIORITY = "optional"
DEPENDS = "pcsc-lite"

PR = "r0"

COMPATIBLE_MACHINE = "am180x-evm"

S = "${WORKDIR}/HandlerTest-${PV}"

SRC_URI = "https://alioth.debian.org/frs/download.php/1602/HandlerTest-${PV}.tar.gz"

do_install() {
    install -d ${D}${bindir}
    install -m 0755 ${S}/Host/handler_test ${D}${bindir}
}

INSANE_SKIP_${PN} = "true"

SRC_URI[md5sum] = "f0913f9805d4e95b4cfcb23aa498833c"
SRC_URI[sha256sum] = "9ff4b7d4ab3a9744df40e8f2152ec408beb5009018db3e8db711a159f7d43d04"
