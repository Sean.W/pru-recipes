DESCRIPTION = "Assembler for PRU firmware"
LICENSE = "TI"

PR = "r0"

SRC_URI = "https://gforge.ti.com/gf/download/frsrelease/552/4622/pasm"

S = ${WORKDIR}

do_install() {
	install -d ${D}${bindir}
	install -m 0755 ${WORKDIR}/pasm ${D}${bindir}
}

NATIVE_INSTALL_WORKS = "1"

INSANE_SKIP_${PN} = "true"
PACKAGE_STRIP = "no"

BBCLASSEXTEND = "native sdk"

SRC_URI[md5sum] = "26ea1454bf27db1d15ffc4e86ec748a9"
SRC_URI[sha256sum] = "d153523b7032dcc4c64df714a78c7b255cc2e7896ad97575f5521529c3809eb2"
