DESCRIPTION = "TI PRU Soft UART firmware"
HOMEPAGE = "https://www.gitorious.org/pru/pru-uart-fw"
LICENSE = "TI"
PRIORITY = "optional"
DEPENDS = "pasm-native"

PR = "r4"

COMPATIBLE_MACHINE = "am180x-evm"

BRANCH ?= "master"

S = "${WORKDIR}/git"

SRCREV = "bccdb271306696132948a360fe039cd240454bcd"

SRC_URI = "git://gitorious.org/pru/pru-uart-fw.git;protocol=git;branch=${BRANCH}"

do_compile() {
    mkdir -p ${S}/bin
    pasm -b -DMCASP0 PRU_SUART_Emulation.p bin/PRU_SUART_Emulation
}

do_install() {
    install -d ${D}${base_libdir}/firmware
    install -m 0644 ${S}/bin/*.bin ${D}${base_libdir}/firmware
}

FILES_${PN} += "${base_libdir}/firmware/*"
