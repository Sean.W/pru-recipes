DESCRIPTION = "Example application to talk to PRU SmartCard using pcsc-lite"
HOMEPAGE = "pending"
LICENSE = "GPLv2+"
PRIORITY = "optional"
DEPENDS = "pcsc-lite"

PR = "r9"

COMPATIBLE_MACHINE = "am180x-evm"

BRANCH ?= "master"

S = "${WORKDIR}/git"

SRCREV = "d2a1525e73e6e05df84c451c74770c4d0d876de9"

SRC_URI = "git://gitorious.org/pru/pru-sc-testapp.git;protocol=git;branch=${BRANCH}"

CFLAGS += "-I${STAGING_INCDIR}/PCSC"
LDFLAGS += "-L${STAGING_LIBDIR} -lpcsclite"

do_install() {
    make DESTDIR=${D}${bindir} install
}
