DESCRIPTION = "TI PRU SmartCard firmware"
HOMEPAGE = "https://www.gitorious.org/pru/pru-sc-fw"
LICENSE = "TI"
PRIORITY = "optional"
DEPENDS = "pasm-native"

PR = "r6"

INITSCRIPT_NAME = "pru-smartcard-clock"
INITSCRIPT_PARAMS = "defaults 99"

inherit update-rc.d

COMPATIBLE_MACHINE = "am180x-evm"

BRANCH ?= "master"

S = "${WORKDIR}/git"

SRCREV = "0e37e2299c6af8164a615bd64d39a17ca3ad582f"

SRC_URI = "git://gitorious.org/pru/pru-sc-fw.git;protocol=git;branch=${BRANCH} \
           file://pru-smartcard-clock"

do_compile() {
    mkdir -p ${S}/bin
    pasm -b -DMCASP0 PRU_SCARD_Emulation.p bin/PRU_SCARD_Emulation
}

do_install() {
    install -d ${D}${base_libdir}/firmware
    install -m 0644 ${S}/bin/*.bin ${D}${base_libdir}/firmware

    # install the initscript to allow configuring the proper eCAP clock
    # for the smartcard.
    install -d ${D}${sysconfdir}/init.d/
    install -c -m 0755 ${WORKDIR}/pru-smartcard-clock ${D}${sysconfdir}/init.d/
}

FILES_${PN} += "${base_libdir}/firmware/*"
