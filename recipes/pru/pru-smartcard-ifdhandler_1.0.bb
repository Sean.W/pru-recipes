DESCRIPTION = "IFDHandler for TI PRU SmartCard driver"
HOMEPAGE = "https://www.gitorious.org/pru/pru-sc-ifdhandler"
LICENSE = "BSD"
PRIORITY = "optional"
DEPENDS = "pcsc-lite"

PR = "r9"

COMPATIBLE_MACHINE = "am180x-evm"

BRANCH ?= "master"

S = "${WORKDIR}/git"

SRCREV = "8fd7c409a26320aba7764481fef808125d8483ea"

SRC_URI = "git://gitorious.org/pru/pru-sc-ifdhandler.git;protocol=git;branch=${BRANCH}"

CFLAGS += "-pthread -I${STAGING_INCDIR}/PCSC"
LDFLAGS += "-L${STAGING_LIBDIR} -lpcsclite"

do_install() {
    install -d ${D}${libdir}/ifd-ti-pru-sc/
    install -m 0644 ${S}/libtiserial.so ${D}${libdir}/ifd-ti-pru-sc/

    # Add the reader.conf file
    install -d ${D}${sysconfdir}/reader.conf.d
    install -m 0644 ${S}/reader.conf ${D}${sysconfdir}/reader.conf.d/
}

FILES_${PN} += "${libdir}/ifd-ti-pru-sc/*.so \
                ${sysconfdir}/reader.conf.d/*"

RCONFLICT_${PN} = "pcsc-lite-collateral"
CONFFILES_${PN} += "${sysconfdir}/reader.conf.d/*"
